import requests,json,os.path
from app import *

botName = "@wongsugih_bot"

def telegramUrl(method):
	token = "**************************************"
	url = "https://api.telegram.org/bot" + token + "/" + method
	return url

def getUpdates(offset):
	requestLastChat =  telegramUrl("getUpdates") + "?offset=" + str(offset)
	req = requests.get(requestLastChat)
	response = req.json()
	if response['ok']:
		return response['result']
	else:
		return "error"

def readResponse(response):
	text_stat = None
	update_id = response["update_id"]
	if "message" not in response: return update_id
	message = response["message"]
	try:
		if "text" in message:
			chatId = message["chat"]["id"]
			message_id = message["message_id"]
			text = message["text"].encode("utf-8")
			responseId = message["chat"]["id"]

			if text[0][0] != "/": return update_id

			if message["chat"]["type"] == "group": chatId = message["from"]["id"]
			
			response_from_bot = create_function_for_bot(chatId, text)
			if response_from_bot != "":
				send_response_for_user(responseId, response_from_bot)
	except NameError:
		print "error :/"
	return update_id

def parseTextMessage(text):
	text = text.split(" ")
	if len(text) > 1:
		text = {
			"command": text[0],
			"param": text[1]
		}
	else:
		text = {
			"command": text[0]
		}
  
	return text

def doCommand(chatId, text):
	global botName
	template_output = ""
	text = parseTextMessage(text)
	if text["command"] == "/help" or text["command"] == "/help"+botName:
		template_output += "------[Menu Help]------\n\n"
		template_output += "*/tuyul [currency|profit|buy|active]*\n"
		template_output += "-> For add or update currency to get notify when Sell price more than Buy price based on your profit set\n"
		template_output += "-> Ex: ADAETH|20|0.0001000|true\n\n"
		template_output += "*/daftarTuyul*\n"
		template_output += "-> To see your config currency was added\n\n"
		template_output += "*/active [currency|active]*\n"
		template_output += "-> To activated notification of your currency\n"
		template_output += "-> Ex: ADAETH|true\n"

	if (text["command"] == "/tuyul" or text["command"] == "/tuyul@wongsugih_bot") and "param" in text:
		print "--- CREATE / UPDATE PLAYER CONFIG ---"
		newConfig = text["param"]
		response = newPlayer(chatId, newConfig)
		if response:
			template_output = """
			*Success added or modified currency*
			"""

	if text["command"] == "/daftartuyul" or text["command"] == "/daftartuyul"+botName:
		if checkPlayerExist(chatId):
			currency = readCurrencyPlayer("%s.json" % chatId)
			template_output = "*Your available currency was set:* \n%s" % currency

	if (text["command"] == "/active" or text["command"] == "/active"+botName) and "param" in text:
		setActive = setActiveNotifCurrency(("%s.json" % chatId), text["param"])
		if setActive:
			template_output = """
			*Success update status currency*
			"""

	# if (text["command"] == "/allprice" or text["command"] == "/allprice"+botName) and "param" in text:
	if (text["command"] == "/allprice" or text["command"] == "/allprice"+botName):
		response = readAllPriceMarket()
		# response = readAllPriceMarket(text["param"])
		if response:
			textMarket = "VIP"
			if("param" in text):
				textMarket = text["param"][0].upper()

			template_output = "*All price in "+ textMarket +" Market:* \n%s" % response
		else:
			template_output = ""


	# if template_output == "": template_output = "*Wrong command! Type /help to see command*"
	
	return template_output

def create_function_for_bot(chatId, text):
	res = doCommand(chatId, text)
	return res

def send_response_for_user(chatId, response_from_bot):
	payload = {
		'chat_id'   : chatId, 
		'text'      : response_from_bot,
		'parse_mode': 'Markdown'
	}
	res = requests.post(telegramUrl("sendMessage"), data=payload)
	return res

def create_session(update_id, log_name):
	file = open(log_name, "wb")
	file.write(str(update_id))
	file.close()

def main():
	global marketData
	

	while(1):
		log_name = "last_update_id_python"
		update_id = 0
		if os.path.exists(log_name):
			fh = open(log_name,"r")
			last_update_id = fh.read()
			update_id = int(last_update_id)

		### NOTIFY CURRENCY WHEN PRICE UP ###
		folders = readfolder()
		print "--- NOTIFY PRICE UP by THRESHOLD ---"
		for file in folders:
			if "json" not in file: continue
			config = readConfigPlayer(file)
			result = compare(config, marketData)
			notifyPlayer(config['id'],result)
		
		response = getUpdates(update_id)
		
		for res in response:
			try:
				update_id = readResponse(res)
			except ValueError:
				print "error :/"
		
		last_update_id = update_id + 1
		create_session(last_update_id, log_name)

if __name__ == "__main__":
	main()
