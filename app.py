import requests,re,json,os,time,locale
from wongsugih_bot import *
from datetime import datetime

playerFolder = "player"
#checkPrice("ETH")

def number_format(num, with_prefix = False, decimal = 2):
	currencyFormat = "Rp. {:,}".format(num)
	if with_prefix:
			return currencyFormat
	return currencyFormat

def getPrice(marketType, typeCurrency = ""):
	if marketType == "VIP":
		# url = "https://vip.bitcoin.co.id/api/%s_idr/ticker" % (typeCurrency.lower())
		url = "https://api2.bitcoin.co.id/api/eth_idr/webdata"
	if marketType == "LUNO":
		typeCurrency = typeCurrency.upper()
		if typeCurrency == "BTC": typeCurrency = "XBT"
		url = "https://www.luno.com/ajax/1/price?pair=%sIDR" % (typeCurrency)
	result = requests.get(url)
	response, httpcode = result.json() , result.status_code
	if httpcode != 200 : notifyError(response)
	return response

def getMarketData():
	result = requests.get('https://www.binance.com/exchange/public/product')
	response, httpcode = result.json() , result.status_code
	if httpcode != 200 : notifyError(response)
	return response

marketData = getMarketData()

def converCurrency(amount,frm,to):
	result = {'status' : 'inputan salah tolol', 'amount' : 0 , 'currency' : {'from' : '', 'to' : ''}}
	get = requests.get('https://finance.google.com/finance/converter?a=%f&from=%s&to=%s&meta=' % (amount,frm,to) )
	response , httpcode = get.text, get.status_code
	if 'class=bld' in response:
		currency = re.findall(r'<span class=bld>(.*?) (.*?)</span>', response, re.M|re.I)
		result = {'status' : 'oke', 'amount' : float(currency[0][0]) ,  'currency' : {'from' : frm, 'to' : to}}
	return result

def checkPrice(symbol):
	#sementara pake binance
	get = requests.get('https://www.binance.com/exchange/public/product?symbol=%sUSDT' % (symbol))
	response , httpcode = get.json(), get.status_code
	data = [data for data in response['data']]
	if len(data) != 0:
		amount = float(data[0]['close'])
		data = converCurrency(amount,"USD","IDR")
		print "1 %s = %.2f USD | %2.f IDR" % (symbol, amount , data['amount'])


def notifyError(response):
	print response

def notifyPlayer(id, result):
	res = ""
	res += "## Player %s ##\n" % id
	ifSend = False
	for x in result:
		if "active" in x[0] and str(x[0]["active"]) == "true":
			res += "Symbol: " + str(x[0]["symbol"]) + " | "
			res += "Close: " + str(x[1]["close"]) + " | "
			res += "Buy: " + str(x[0]["buy"]) + "\n"
			setActiveNotifCurrency(("%s.json" % id), str(x[0]["symbol"]) + "|false")
			ifSend = True
	res += "\n"

	if ifSend:
		send_response_for_user(id, res)
		print "%s" % res

def readfolder():
	global playerFolder
	return os.listdir(playerFolder)

def readConfigPlayer(file):
	global playerFolder
	return json.load(open("%s/%s" % (playerFolder, file)))

def readCurrencyPlayer(file):
	config = readConfigPlayer(file)
	textResponse = ""
	for currency in config['currency']:
		threshold = float(str(currency["threshold"])) * 100
		textResponse += "Symbol: " + str(currency["symbol"]) + " | "
		textResponse += "Threshold: " + str(threshold) + " | "
		textResponse += "Buy: " + str(currency["buy"]) + " | "
		textResponse += "Active: " + str(currency["active"]) + "\n"
	return textResponse

def compareSingle(config, marketData):
	buy = float(config['buy'])
	close = float(marketData['close'])
	threshold = (float(config['threshold']) + 1)

	if config['symbol'] != marketData['symbol']: return False
	if (buy * threshold )< close: return (config, marketData)
	return False

def compare(config, marketData):
	placeCurrency = []
	for currency in config['currency']:
		for m in marketData['data']:
			placeCurrency.append(compareSingle(currency, m))
	return list(filter(lambda x: x != False, placeCurrency))

def checkPlayerExist(id):
	global playerFolder
	playerExist = os.path.isfile("%s/%s.json" % (playerFolder , id))
	if playerExist: return True
	return False

def changePlayerConfig(file, newConfig, marketData):
	global playerFolder
	config = readConfigPlayer(file)
	newConfigPlayer = newConfig.split('|')
	if len(newConfigPlayer) != 4: return False
	
	newConfigPlayer[0] = newConfigPlayer[0].upper()
	symbol = filter(lambda x: x["symbol"] == newConfigPlayer[0], config["currency"])
	checkMarketAvaliable = filter(lambda x: x["symbol"] == newConfigPlayer[0], marketData["data"])
	if len(checkMarketAvaliable) == 0: return False
	if len(symbol) > 0:
		config["currency"] = filter(lambda x: x["symbol"] != newConfigPlayer[0], config["currency"])
		symbol[0]["threshold"] = float(newConfigPlayer[1]) / float(100)
		symbol[0]["buy"] = float(newConfigPlayer[2])
		config["currency"].append(symbol[0])
	else:
		symbol = {
			"threshold": float(newConfigPlayer[1]) / float(100),
			"buy": newConfigPlayer[2],
			"symbol": newConfigPlayer[0].upper()
		}
		config["currency"].append(symbol)
	with open("%s/%s" % (playerFolder, file), "w") as outfile:  
		conf = json.dumps(config, indent=2, sort_keys=True)
		outfile.write(conf)
	return True

def createPlayerConfig(id, file, newConfig, marketData):
	global playerFolder
	newConfigPlayer = newConfig.split('|')
	if len(newConfigPlayer) != 4: return False

	newConfigPlayer[0] = newConfigPlayer[0].upper()
	config = {
		"id": id,
		"currency": [
			{
				"threshold": float(newConfigPlayer[1]) / float(100),
				"buy": newConfigPlayer[2],
				"symbol": newConfigPlayer[0].upper(),
				"active": True
			}
		]
	}
	checkMarketAvaliable = filter(lambda x: x["symbol"] == newConfigPlayer[0], marketData["data"])
	if len(checkMarketAvaliable) > 0:
		with open("%s/%s" % (playerFolder, file), "w") as outfile:  
			# json.dump(config, outfile)
			conf = json.dumps(config, indent=2, sort_keys=True)
			outfile.write(conf)
		return True
	return False

def setActiveNotifCurrency(file, newConfig):
	config = readConfigPlayer(file)
	newConfigPlayer = newConfig.split('|')
	if len(newConfigPlayer) != 2: return False
	newConfigPlayer[0] = newConfigPlayer[0].upper()
	symbol = filter(lambda x: x["symbol"] == newConfigPlayer[0], config["currency"])
	if len(symbol) > 0:
		config["currency"] = filter(lambda x: x["symbol"] != newConfigPlayer[0], config["currency"])
		symbol[0]["active"] = newConfigPlayer[1]
		config["currency"].append(symbol[0])
	with open("%s/%s" % (playerFolder, file), "w") as outfile:
		conf = json.dumps(config, indent=2, sort_keys=True)
		outfile.write(conf)
	return True

def newPlayer(id, newConfig):
	global marketData
	file = "%s.json" % id
	playerExist = checkPlayerExist(id)
	result = ""
	if playerExist: result = changePlayerConfig(file, newConfig, marketData)
	else: result = createPlayerConfig(id, file, newConfig, marketData)
	
	if result: 
		print "Create / Update Player %s Config Success" % id 
		return True
	else: 
		print "Wrong Commands / Params Input" 
		return False

def fetchResponseVIPMarket(data):
	prices = data["prices"]
	textResponse = ""

	for x in prices:
		if "idr" not in x: continue
		textResponse += str(x).upper() + ": *" + number_format(float(prices[x])) + "*\n"
	
	return textResponse

def readAllPriceMarket(param = ""):
	if(param != ""):
		param = param.split('|')
		marketType = param[0]
		textResponse = ""
		
		if marketType == "LUNO":
			response = getPrice(marketType)
			return textResponse
			##UNDERCONSTRUCTION
	
	# if marketType == "VIP":
	response = getPrice("VIP")
	textResponse = fetchResponseVIPMarket(response)
	
	return textResponse
	
