import requests,json,os.path
from app import *

def readWalletPlayer(file):
	display = 0
	wallet = file['wallet']
	if display:
		for key,value in wallet.items(): 
			totalCrypto = sum(map(lambda x: x['total'] ,value['transaction']))
			print "%s = %.2f " % ( key, totalCrypto)
	return wallet

def calculateWallet(chooseMarketPlace, listChoose):
	marketName = chooseMarketPlace['name']
	### NOTIFY CURRENCY WHEN PRICE UP ###
	folders = readfolder()
	for file in folders:
		config = readConfigPlayer(file)
		data = readWalletPlayer(config)

		totalKeuntunganKotor = 0
		totalKeuntunganBersih = 0
		totalModalKeseluruhan = 0
		totalAsset = 0
		for choose in listChoose:
			rules = ['ETH', 'BTC'] #LUNO ONLY , ETH & BTC
			buy = list(filter(lambda x: x["type"] == "BUY", data[choose]["transaction"]))
			# soon for this feature #
			#deposit = list(filter(lambda x: x["type"] == "DEPOSIT", data[choose]["transaction"]))
			#sell = list(filter(lambda x: x["type"] == "SELL", data[choose]["transaction"]))
			#fee = list(filter(lambda x: x["type"] == "FEE", data[choose]["transaction"]))
			if marketName == "LUNO" and choose not in rules: buy = []
			#	print "%s | %s" % (marketName, choose)
			if len(buy) >= 1:
				totalModal = sum(map(lambda x: x['price'] ,buy))  #total modal
				totalCrypto = sum(map(lambda x: x['total'] ,buy))
				marketPrice = getPrice(choose, marketName)
				hargaPasar =  float(marketPrice['ticker']['last']) if marketName == "VIP" else float(marketPrice['price']) 					
				resultPredict = ( hargaPasar * totalCrypto)
				keuntunganKotor = (resultPredict - totalModal)

				if (resultPredict > totalModal):					
					keuntunganBersih = ( keuntunganKotor - chooseMarketPlace['fee'] )
					#status = "KEUNTUNGAN KOTOR: %.2f | KEUNTUNGAN BERSIH [FEE (-%.2f)]: %.2f" % ( keuntunganKotor, chooseMarketPlace['fee'], keuntunganBersih)
					status = "KEUNTUNGAN KOTOR: %.2f " % ( keuntunganKotor)
					totalKeuntunganBersih = totalKeuntunganBersih + keuntunganKotor
					totalKeuntunganKotor = totalKeuntunganKotor + keuntunganBersih
				else:
					status = "KERUGIAN : %2.f" % ( (totalModal - resultPredict) )
				print "[%s] TOTAL %s: %.2f | TOTALMODAL: %d | HARGAPASAR: %d | TOTALDANA: %d | %s" % ( chooseMarketPlace['name'], choose, totalCrypto, totalModal, hargaPasar,( totalCrypto * hargaPasar) ,status)
				totalAsset = totalAsset + ( keuntunganKotor + totalModal )
				totalModalKeseluruhan = totalModalKeseluruhan + totalModal

			
		#print "TOTAL MODAL KESELURUHAN: %.2f | TOTAL Nilai ASSET: %.2f | TOTAL KOTOR: %.2f | TOTAL BERSIH: %.2f" % (totalModalKeseluruhan, totalAsset, totalKeuntunganKotor, totalKeuntunganBersih)
		print "TOTAL MODAL KESELURUHAN: %.2f | TOTAL Nilai ASSET: %.2f | TOTAL KEUNTUNGAN KOTOR: %.2f" % (totalModalKeseluruhan, totalAsset, (totalAsset - totalModalKeseluruhan ))


def main():
	global marketData
	listChoose= ["NXT", "XRP", "ETH" ] #misal pilih
	#untuk fee masih hardcode
	marketPlace= [ { 'name' : 'VIP' , 'fee' : 25000 } , { 'name' : 'LUNO' , 'fee' : 15000 }]
	print "--- THIS IS YOUR'S LIST CRYPTOCURRENCY ---"
	print "--- BASED ON VIP & LUNO MARKETPLACE ---"
	for chooseMarketPlace in marketPlace:
		calculateWallet(chooseMarketPlace, listChoose)

if __name__ == "__main__":
	main()