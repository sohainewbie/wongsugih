FROM python:2
WORKDIR ./python/wongsugih
COPY ./ ./
RUN pip install --upgrade pip
RUN pip install requests
CMD [ "python", "./wongsugih_bot.py" ]
